import * as dotenv from 'dotenv';
dotenv.config();
let pair = process.env.PAIR;
export default {
    telegram: {
        token: process.env.TELEGRAM_TOKEN,
        chat_id: process.env.TELEGRAM_CHATID,
    },
    pair,
    binance_url: `https://www.binance.com/en/futures/${pair.toUpperCase()}`,
    min_percent: process.env.MIN_PERCENT || 51,
}