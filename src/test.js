import CandyService from "./services/candyService";
import test from 'test';

async function mainTest() {

    test('isBiggerThenMiddle', async t => {
        let service = new CandyService(true);
        service.middles.day = 100;
        service.last_threshold = 64;
        let is = service.isBiggerThenMiddle(200);

        assert.assertBoolean(is);

    });

    console.log(CandyService)
}

mainTest();