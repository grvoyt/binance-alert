import express from 'express';
import TelegramService from "./services/telegramService";
import createError from 'http-errors';

main();

async function main() {
    const app = express();
    const port = 3000;

    let telegram = new TelegramService('BTCUSDT');

    app.post('/tradingview/buy', async (req,res) => {
        await telegram.sendMessage('Buy LONG signal');
        res.send({status: 'ok'});
    })

    app.post('/tradingview/sell', async (req,res) => {
        await telegram.sendMessage('Sell Signal SHORT');
        res.send({status: 'ok'});
    })

    app.use(function(req, res, next) {
        res.status(404).send('404');
    });

    app.listen(port, () => console.log(`Express app listening on port ${port}`))
}