export default class Kline {
    constructor(data) {
        this.openTime = new Date(data[0]);
        this.open = parseFloat(data[1]);
        this.high = parseFloat(data[2]);
        this.low = parseFloat(data[3]);
        this.close = parseFloat(data[4]);
        this.volume = parseFloat(data[5])
        this.closeTime = new Date(data[6]);
        this.quoteAssetVolume = parseFloat(data[7]);
        this.numberOfTrades = parseFloat(data[8]);
        this.takerBuyBaseAssetVolume = parseFloat(data[9]);
        this.takerBuyQuoteAssetVolume = parseFloat(data[10]);
        this.ignore = data[11];
    }
}