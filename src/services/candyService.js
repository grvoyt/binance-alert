import WebsocketService from "./websocketService";
import DataKlines from "./dataKlines";
import Telegram from "./telegramService";
import {CronJob} from "cron";
import CreateLogger from "./logger";
import config from "../config";
import moment from "moment";

class CandyService {
    pair;
    middles = {
        month: 0.0,
        week: 0.0,
        day: 0.0,
    }

    message_id = null;

    threshold;

    now_volume = 0;

    last_volume = 0;

    last_threshold = 0;

    can_send = true;

    constructor(pair,is_market,debug = false) {
        this.pair = pair;

        this.ws = new WebsocketService(pair,is_market);

        this.cron = new CronJob('0 1 * * * *', this.changeMiddles.bind(this), null, true, 'Europe/Moscow');

        this.logger = CreateLogger('candy');

        this.telegram = new Telegram(pair,debug);

        this.threshold = config.min_percent;

        setInterval( () => {
            console.log(
                moment().format('HH:mm:ss'),
                'can_send',
                this.can_send,
                true
            )
            this.can_send = true;
        },4 * 1000);

        this.logger.log('constructor','connected');
    }

    async changeMiddles() {
        await this.getPeriods();
    }

    async start() {
        //get Data by periods
        await this.getPeriods();

        //Start Websocket
        this.ws.on('message', this.handlePicker.bind(this))
        this.ws.start();

        this.telegram.sendMessage(`Погнали! ${this.pair}`);
        this.logger.log('ws started');
    }

    async getPeriods() {
        let dataKlines = new DataKlines(this.pair);
        this.middles = await dataKlines.getData()
        this.logger.log('getPeriods',this.middles)
    }

    handlePicker({volume,is_closed,dif}) {
        console.log('handlePicker',volume,this.middles.day.toFixed(2),this.last_threshold.toFixed(2),this.message_id,dif,is_closed);
        //Проверяем превышение порога дневного
        if( !this.isBiggerThenMiddle(volume) )  return;
        if( dif === 'red' ) return;

        this.now_volume = volume;

        this.logger.log({
            volume,
            last_threshold: this.last_threshold,
        })

        if( is_closed ) {
            this.message_id = null;
        } else {
            this.updateMessageTelegram()
        }
    }

    isBiggerThenMiddle(volume) {
        let temp = this.calcPercent(volume,this.middles.day);

        if( this.last_threshold > temp ) {
            return false;
        } else {
            this.last_threshold = temp;
        }

        return this.last_threshold > this.threshold;
    }

    async telegramSendMessage() {
        let message = 'Запускаюсь...';
        this.message_id = await this.telegram.sendMessage(message);

    }

    async updateMessageTelegram() {

        if( !this.can_send ) return;

        this.can_send = false;

        let message = this.generateMessage();
        if( this.message_id ) {

            if( this.last_volume === this.now_volume ) return;

            await this.telegram.updateMessaage(this.message_id,message)
        } else {
            this.message_id = await this.telegram.sendMessage(message);
        }
    }

    generateMessage() {
        let month = this.calcPercent(this.now_volume,this.middles.month);
        let week = this.calcPercent(this.now_volume,this.middles.week);
        let day = this.calcPercent(this.now_volume,this.middles.day);
        const month_volume = this.formatVolume(this.middles.month);
        const week_volume = this.formatVolume(this.middles.week);
        const day_volume = this.formatVolume(this.middles.day);
        const now_volume = this.formatVolume(this.now_volume);

        let message = '';

        message += `🚀 <b>${this.pair}</b> 🚀\n\n`;
        message += `Месяц: ${month_volume} <b>(${month.toFixed(2)}%)</b>\n`;
        message += `Неделя: ${week_volume} <b>(${week.toFixed(2)}%)</b>\n`;
        message += `День: ${day_volume} <b>(${day.toFixed(2)}%)</b>\n\n`;
        message += `Сейчас: <b>${now_volume}</b>`;
        return message;
    }

    calcPercent(needle,from) {
        let percent = (needle * 100) / from;
        return percent - 100;
    }

    formatVolume(float_value) {
        let volume = Math.round(float_value / 100);
        return (volume*100).toLocaleString('en-US');
    }
}


export default CandyService;