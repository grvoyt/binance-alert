import axios from "axios";
import config from "../config";
import _ from 'lodash';
import Kline from "../dto/kline";
import moment from "moment";

export default class DataKlines {
    pair;
    klines_url = 'https://fapi.binance.com/fapi/v1/klines';
    ping_url = 'https://fapi.binance.com/fapi/v1/ping';

    periods = [
        {
            type: 'month',
            value: 60480
        },
        {
            type: 'week',
            value: 7 * 288
        },
        {
            type: 'day',
            value: 288
        }
    ];

    constructor(pair) {
        this.pair = pair;
    }

    async getData() {
        let data = await Promise.all([
            this.getByMonth(),
            this.getByWeek(),
            this.getByDay(),
        ]);

        let result = {};
        for( let i in this.periods ) {
            let kline = data[i];

            let {type,value} = this.periods[i];

            if( type === 'month' ) {
                let count_days = moment().subtract(1,'month').endOf('month').format('DD');
                value = parseInt(count_days) * 288;
            }

            let middle_volume = (kline.volume / value).toFixed(2);
            result[type] = parseFloat(middle_volume);

        }
        return result;
    }

    async getByMonth() {
        let query = {
            interval: '1M',
            startTime: moment().subtract(1,'month').startOf('month').valueOf(),
            endTime: moment().subtract(1,'month').endOf('month').valueOf()
        };

        return await this.requestToBinance(query);
    }

    async getByWeek() {
        let query = {
            interval: '1w',
            startTime: moment().subtract(1,'week').startOf('isoWeek').valueOf(),
            endTime: moment().startOf('isoWeek').valueOf(),
        };

        return await this.requestToBinance(query);
    }

    async getByDay() {
        let query = {
            interval: '1d',
            startTime: moment().subtract(1,'day').startOf('day').valueOf(),
            endTime: moment().startOf('day').valueOf(),
        };

        return await this.requestToBinance(query);
    }

    async requestToBinance(query) {
        let searchParams = new URLSearchParams({
            ...query,
            symbol: this.pair,
        })
        let url = `${this.klines_url}?${searchParams.toString()}`

        try {
            let {data} = await axios.get(url);
            let collection = _.map(data, item => new Kline(item))
            return _.first(collection);
        } catch (error) {
            if( error.response ) {
                console.log(`${error.response.status} - ${url}`)
            }
            return null
        }
    }
}