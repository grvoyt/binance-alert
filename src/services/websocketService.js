import WebSocket from "ws";
import config from "../config";
import moment from "moment";
import CreateLogger from "./logger";

class WebsocketService {
    socket;
    ws_url;

    /**
     * процент превышения
     * @type {number}
     */
    percent_limit = 50;

    count = 1;

    middle_volume = 3200;

    volume_sum = 3200;

    logger;

    dif = 0;

    percent = 0;

    eventHandlers = {};

    constructor(pair,is_market) {
        let prefix = is_market ? 'stream' : 'fstream';
        this.ws_url = `wss://${prefix}.binance.com/stream?streams=${pair}@kline_5m`;

        //this.logger = CreateLogger('websocket');
    }

    start() {
        console.log('try to connect')

        this.socket = new WebSocket(this.ws_url);

        this.socket.on('open', this._open.bind(this));
        this.socket.on('close', this._close.bind(this));
        this.socket.on('message', this._message.bind(this));
    }

    async _message(buffer) {
        //process.stdout.write('\x1b[H\x1b[2J')

        const {data} = JSON.parse(buffer.toString());

        // const time = new Date(data.E)
        // const time_start = new Date(data.k.t)
        // const time_end = new Date(data.k.T)
        let is_closed = data.k.x;
        const open = data.k.o;
        const close = data.k.c;
        let dif = this.dif = (close - open) < 0 ? 'red' : 'green';
        const volume = parseInt(data.k.v);

        let callbacks = this.eventHandlers['message'];

        callbacks.forEach(callback => callback({
            volume,
            is_closed: data.k.x,
            dif
        }));
    }

    // log(message) {
    //     const date = moment().format('YYYY-MM-DD HH:mm:ss');
    //     this.logger.log(`[${date}]:`,message)
    // }

    _open() {
        //process.stdout.write('\x1b[H\x1b[2J')
        console.log('connected, wait klines');
    }

    _close() {
        console.log('websocket closed')
    }

    on(type,handler) {
        if (!this.eventHandlers[type]) { this.eventHandlers[type] = []; }
        this.eventHandlers[type].push(handler);
        return this;
    }
}

export default WebsocketService;