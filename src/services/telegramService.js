import config from '../config';
import axios from 'axios';

class Telegram {
    url

    chat_id;

    debug = false;

    pair;

    constructor(pair,debug = false) {
        this.pair = pair
        this.url = `https://api.telegram.org/bot${config.telegram.token}`;
        this.chat_id = config.telegram.chat_id;
        this.debug = debug;
    }

    async sendMessage(text) {
        return await this.request('sendMessage',text);
    }

    async updateMessaage(message_id, text) {
        return await this.request('editMessageText',text,message_id);
    }

    async request(path,text,message_id = null) {
        const binance_url = `https://www.binance.com/en/futures/${this.pair.toUpperCase()}`
        text += `\n<a href="${binance_url}">${this.pair}</a>`;
        try {
            let meesage = {
                chat_id: this.chat_id,
                message_id,
                text,
                parse_mode: 'HTML',
                disable_web_page_preview: true
            };

            if( !!message_id ) {
                meesage.message_id = message_id;
            }

            const url = `${this.url}/${path}`;

            if( this.debug ) {
                console.log(url,meesage)
                return 1;
            } else {
                let { data } = await axios.post(url,meesage);
                return data.result.message_id || null;
            }
        } catch (e) {
            console.log(
                'ERROR',
                `${e.response.code || 0} - ${e.response.data.description || ''}`
            )
        }
    }
}

export default Telegram;