import {Console} from "node:console";
import fs from "fs";

function createLogger(file_name) {
    return new Console({
        stdout: fs.createWriteStream(
            `./log/${file_name}.log`,
            //{flags: 'a'}
        ),
        //stderr: fs.createWriteStream("./log/websocket-errors.txt"),
    });
}

export default createLogger;