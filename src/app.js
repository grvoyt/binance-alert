import CandyService from "./services/candyService";

main();
async function main() {
    const pair = process.argv[2];
    const is_market = parseInt(process.argv[3]) === 1;
    console.log(`Start service for: ${pair} - ${is_market ? 'market' : 'future'}`);
    let service = new CandyService(pair,is_market);
    service.start();
}

