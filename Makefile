build:
	npm install
	rm -rf dist
	npm run build
start:
	npm run start

clear:
	rm -rf ./log/* ./dist

update:
	git pull
	npm install
	rm -rf dist
	npm run build
